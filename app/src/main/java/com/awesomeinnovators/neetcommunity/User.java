package com.awesomeinnovators.neetcommunity;

public class User {
    public String id;
    public String userName;
    public String email;
    public String password;
    public String token;
    public String avatar_url;
    public String display_name;
    /* display_name is for displaying to the general public */

    public User(String id, String userName, String email, String password) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.token = null;
        this.avatar_url = null;
        
    }

}